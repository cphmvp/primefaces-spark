/*
 * Copyright 2009-2015 PrimeTek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.primefaces.component.menubar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.FacesException;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;

import org.primefaces.component.api.AjaxSource;
import org.primefaces.component.api.UIOutcomeTarget;
import org.primefaces.component.menu.AbstractMenu;
import org.primefaces.component.menu.BaseMenuRenderer;
import org.primefaces.model.menu.BaseMenuModel;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuItem;
import org.primefaces.model.menu.Separator;
import org.primefaces.model.menu.Submenu;
import org.primefaces.util.ComponentTraversalUtils;
//import org.primefaces.util.WidgetBuilder;

@FacesRenderer(componentFamily = "org.primefaces.component",
        rendererType = "org.primefaces.component.SparkMenubarRenderer")
public class SparkMenubarRenderer extends BaseMenuRenderer {

    @Override
    protected void encodeScript(FacesContext context, AbstractMenu abstractMenu) throws IOException {
//        Menubar menubar = (Menubar) abstractMenu;
//        String clientId = menubar.getClientId(context);
//
//        WidgetBuilder wb = getWidgetBuilder(context);
//        wb.init("Menubar", menubar.resolveWidgetVar(), clientId)
//                .attr("autoDisplay", menubar.isAutoDisplay())
//                .attr("toggleEvent", menubar.getToggleEvent(), null);
//
//        wb.finish();
        // no-op
        // autoDisplay与toggleEvent这两个属性，对于Spark的菜单不起作用
    }

    @Override
    protected void encodeMarkup(FacesContext context, AbstractMenu abstractMenu) throws IOException {
        Menubar menubar = (Menubar) abstractMenu;
        String style = menubar.getStyle();
        String styleClass = menubar.getStyleClass();
        final String CONTAINER_CLASS = "BordRad3 Unselectable";
        styleClass = styleClass == null ? CONTAINER_CLASS : CONTAINER_CLASS + " " + styleClass;

        encodeMenu(context, menubar, style, styleClass, "menubar");
    }

    protected void encodeMenu(FacesContext context, AbstractMenu menu, String style, String styleClass, String role) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        UIComponent optionsFacet = menu.getFacet("options");

        writer.startElement("ul", menu);
//        writer.writeAttribute("id", menu.getClientId(context), "id");
        writer.writeAttribute("id", "layout-menu", "id"); // ID使用硬编码赋值
        writer.writeAttribute("class", styleClass, "styleClass");
        writer.writeAttribute("tabindex", menu.getTabindex(), null);

        if (style != null) {
            writer.writeAttribute("style", style, "style");
        }
        writer.writeAttribute("role", "menubar", null);

        encodeKeyboardTarget(context, menu);

        if (menu.getElementsCount() > 0) {
            int menuLevel = -1;
            encodeElements(context, menu, menu.getElements(), menuLevel);
        }

        if (optionsFacet != null) {
            writer.startElement("li", null);
            writer.writeAttribute("style", "float:right", null);
            writer.writeAttribute("role", "menuitem", null);
            optionsFacet.encodeAll(context);
            writer.endElement("li");
        }

        writer.endElement("ul");
    }

    protected void encodeElements(FacesContext context, AbstractMenu menu, List<MenuElement> elements, int menuLevel) throws IOException {
        ResponseWriter writer = context.getResponseWriter();

        for (MenuElement element : elements) {
            if (element.isRendered()) {
                if (element instanceof MenuItem) {
                    MenuItem menuItem = (MenuItem) element;
                    String containerStyle = menuItem.getContainerStyle();
                    String containerStyleClass = menuItem.getContainerStyleClass();

                    writer.startElement("li", null);
                    writer.writeAttribute("id", menuItem.getClientId(), null);
                    writer.writeAttribute("class", containerStyleClass, null);
                    writer.writeAttribute("role", "menuitem", null);
                    if (containerStyle != null) {
                        writer.writeAttribute("style", containerStyle, null);
                    }
                    encodeMenuItem(context, menu, menuItem, menuLevel);
                    writer.endElement("li");
                } else if (element instanceof Submenu) {
                    Submenu submenu = (Submenu) element;
                    String style = submenu.getStyle();
                    String styleClass = submenu.getStyleClass();

                    writer.startElement("li", null);
                    writer.writeAttribute("id", submenu.getClientId(), null);
                    writer.writeAttribute("class", styleClass, null);
                    if (style != null) {
                        writer.writeAttribute("style", style, null);
                    }
                    writer.writeAttribute("role", "menuitem", null);
                    encodeSubmenu(context, menu, submenu, menuLevel);
                    writer.endElement("li");
                } else if (element instanceof Separator) {
                    encodeSeparator(context, (Separator) element);
                }
            }
        }
    }

    protected void encodeMenuItem(FacesContext context, AbstractMenu menu, MenuItem menuitem, int menuLevel) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        String title = menuitem.getTitle();

        if (menuitem.shouldRenderChildren()) {
            renderChildren(context, (UIComponent) menuitem);
        } else {
            boolean disabled = menuitem.isDisabled();
            String style = menuitem.getStyle();

            writer.startElement("a", null);
            writer.writeAttribute("tabindex", "-1", null);
            if (shouldRenderId(menuitem)) {
                writer.writeAttribute("id", menuitem.getClientId(), null);
            }
            if (title != null) {
                writer.writeAttribute("title", title, null);
            }

            String styleClass = this.getLinkStyleClass(menuitem);
            if (menuLevel > 0) {
                styleClass += " menuLevel" + menuLevel;
            }
            if (disabled) {
                styleClass += " ui-state-disabled";
            }

            writer.writeAttribute("class", styleClass, null);

            if (style != null) {
                writer.writeAttribute("style", style, null);
            }

            if (disabled) {
                writer.writeAttribute("href", "#", null);
                writer.writeAttribute("onclick", "return false;", null);
            } else {
                setConfirmationScript(context, menuitem);
                String onclick = menuitem.getOnclick();

                // GET
                if (menuitem.getUrl() != null || menuitem.getOutcome() != null) {
                    String targetURL = getTargetURL(context, (UIOutcomeTarget) menuitem);
                    writer.writeAttribute("href", targetURL, null);

                    if (menuitem.getTarget() != null) {
                        writer.writeAttribute("target", menuitem.getTarget(), null);
                    }
                } else { // POST
                    writer.writeAttribute("href", "#", null);

                    //UIComponent form = ComponentUtils.findParentForm(context, menu); // PrimeFaces-5.2
                    UIComponent form = ComponentTraversalUtils.closestForm(context, menu); // PrimeFaces-5.3
                    if (form == null) {
                        throw new FacesException("MenuItem must be inside a form element");
                    }

                    String command;
                    if (menuitem.isDynamic()) {
                        String menuClientId = menu.getClientId(context);
                        Map<String, List<String>> params = menuitem.getParams();
                        if (params == null) {
                            params = new LinkedHashMap<String, List<String>>();
                        }
                        List<String> idParams = new ArrayList<String>();
                        idParams.add(menuitem.getId());
                        params.put(menuClientId + "_menuid", idParams);

                        command = menuitem.isAjax() ? buildAjaxRequest(context, menu, (AjaxSource) menuitem, form, params) : buildNonAjaxRequest(context, menu, form, menuClientId, params, true);
                    } else {
                        command = menuitem.isAjax() ? buildAjaxRequest(context, (AjaxSource) menuitem, form) : buildNonAjaxRequest(context, ((UIComponent) menuitem), form, ((UIComponent) menuitem).getClientId(context), true);
                    }

                    onclick = (onclick == null) ? command : onclick + ";" + command;
                }

                if (onclick != null) {
                    if (menuitem.requiresConfirmation()) {
                        writer.writeAttribute("data-pfconfirmcommand", onclick, null);
                        writer.writeAttribute("onclick", menuitem.getConfirmationScript(), "onclick");
                    } else {
                        writer.writeAttribute("onclick", onclick, null);
                    }
                }
            }

            encodeMenuItemContent(context, menu, menuitem);

            writer.endElement("a");
        }
    }

    @Override
    protected String getLinkStyleClass(MenuItem menuItem) {
        String styleClass = menuItem.getStyleClass();

        final String MENUITEM_LINK_CLASS = "Animated05";
        return (styleClass == null) ? MENUITEM_LINK_CLASS : MENUITEM_LINK_CLASS + " " + styleClass;
    }

    @Override
    protected void encodeMenuItemContent(FacesContext context, AbstractMenu menu, MenuItem menuitem) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        String icon = menuitem.getIcon();
        Object value = menuitem.getValue();

        if (icon != null) {
            writer.startElement("i", null);
            writer.writeAttribute("class", icon, null);
            writer.endElement("i");
        }

        if (value != null) {
            writer.writeText(" " + value, "value");
        }
    }

    protected void encodeSubmenu(FacesContext context, AbstractMenu menu, Submenu submenu, int menuLevel) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        String icon = submenu.getIcon();
        String label = submenu.getLabel();

        //title
        writer.startElement("a", null);
        writer.writeAttribute("href", "#", null);
        writer.writeAttribute("onclick", "return false;", null);
        final String SUBMENU_LINK_CLASS = "Animated05 CursPointer";
        String styleClass = (menuLevel > 0 ? SUBMENU_LINK_CLASS + " menuLevel" + menuLevel : SUBMENU_LINK_CLASS);
        writer.writeAttribute("class", styleClass, null);
        writer.writeAttribute("tabindex", "-1", null);

        if (icon != null) {
            writer.startElement("i", null);
            writer.writeAttribute("class", icon, null);
            writer.endElement("i");
        }

        if (label != null) {
            writer.writeText(" " + label, "value");
        }

        encodeSubmenuIcon(context, submenu);

        writer.endElement("a");

        //submenus and menuitems
        if (submenu.getElementsCount() > 0) {
            writer.startElement("ul", null);
            String TIERED_CHILD_SUBMENU_CLASS = (menuLevel < 0 ? "Animated03 submenu" : "submenu");
            writer.writeAttribute("class", TIERED_CHILD_SUBMENU_CLASS, null);
            writer.writeAttribute("role", "menu", null);
            encodeElements(context, menu, submenu.getElements(), ++menuLevel);
            writer.endElement("ul");
        }
    }

    protected void encodeSubmenuIcon(FacesContext context, Submenu submenu) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        Object parent = submenu.getParent();
        String icon = null;

        if (parent == null) {
            icon = (!submenu.getId().contains(BaseMenuModel.ID_SEPARATOR)) ? "fa fa-chevron-down Fs14 Fright ShowOnMobile" : "fa fa-chevron-down Fs12 Fright";
        } else {
            icon = (parent instanceof Menubar) ? "fa fa-chevron-down Fs14 Fright ShowOnMobile" : "fa fa-chevron-down Fs12 Fright";
        }

        writer.startElement("i", null);
        writer.writeAttribute("class", icon, null);
        writer.endElement("i");
    }

    @Override
    protected void encodeSeparator(FacesContext context, Separator separator) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        String style = separator.getStyle();
        String styleClass = separator.getStyleClass();
        final String SEPARATOR_CLASS = "fa fa-ellipsis-v menu-separator";
        styleClass = styleClass == null ? SEPARATOR_CLASS : SEPARATOR_CLASS + " " + styleClass;

        //title
        writer.startElement("li", null);
        writer.writeAttribute("class", styleClass, null);
        if (style != null) {
            writer.writeAttribute("style", style, null);
        }

        writer.endElement("li");
    }
}
